import { Element, ElementType, LanguagePriorities } from '../types.ts'
import findArrangement from '../graph/index.ts'
import { assertArrayIncludes, assertEquals } from "https://deno.land/std@0.167.0/testing/asserts.ts";


type Lang = string
type ElementId = string
interface TestConfiguration {
  title?: string
  stageLanguage: Lang
  elements: Element[]
  languagePriorities: LanguagePriorities
  expectLinks: Array<[ElementId, ElementId, string?]>
}

export async function test(t: Deno.TestContext, conf: TestConfiguration) {
  // deno-lint-ignore require-await
  await t.step(conf.title || `${conf.stageLanguage} spoken on stage`, async () => {
    const elements: Element[] = [
      {
        id: 'stage',
        type: ElementType.Stage,
        langs: [{provides: conf.stageLanguage}]
      },
      ...conf.elements
    ]
    const graph = findArrangement(elements, conf.languagePriorities)

    // collect provider
    const providers: {[lang: string]: string[]} = {}
    for(const el of graph) {
        if(!el.provides) continue
        if(!providers[el.provides]) providers[el.provides] = []
        if(!providers[el.provides].includes(el.id)) providers[el.provides].push(el.id)
    }

    // map input to outputs
    const outputMapping: {[output:string]: string[]} = {}
    for(const el of graph) {
        if(!el.accepts) continue
        if(!providers[el.accepts]) continue
        outputMapping[el.id] = providers[el.accepts]
    }
    try {
      for(let expectedLink of conf.expectLinks) {
        const [from, to, msg] = expectedLink
        assertArrayIncludes(outputMapping[to] || [], [from], msg ? msg.replace(/\{to\}/, to) : `expected link ${from} -> ${to} is missing`)
      }
    } catch(err) {
      console.log(graph)
      throw err
    }
  })
}


