import { test } from "./helper.ts";
import { Element, ElementType, LanguagePriorities } from '../types.ts'

const defaultLanguagePriorities: LanguagePriorities = {
  de: 1.2,
  en: 1.2,
  fr: 1.1,
  es: 1,
  ar: 0.9,
  default: 0.8
}

const TRANSMITTER_DE_EN_ES_FR: Element[] = [
  {
    id: "tx1",
    type: ElementType.Transmitter,
    langs: [{ accepts: 'de' }]
  },
  {
    id: "tx2",
    type: ElementType.Transmitter,
    langs: [{ accepts: 'en' }]
  },
  {
    id: "tx3",
    type: ElementType.Transmitter,
    langs: [{ accepts: 'es' }]
  },
  {
    id: "tx4",
    type: ElementType.Transmitter,
    langs: [{ accepts: 'fr' }]
  },
]

Deno.test("basic common configuration", async (t) => {
  const elements: Element[] = [
    {
      id: "int1",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'de', provides: 'en' }
      ]
    },
    {
      id: "int2",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'de' }
      ]
    },
    {
      id: "int3",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'es' },
        { accepts: 'de', provides: 'es' },
        { accepts: 'es', provides: 'de' }
      ]
    },
    ...TRANSMITTER_DE_EN_ES_FR
  ]

  await test(t, {
    stageLanguage: 'de',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx1'],
      ['stage', 'int1'],
      ['int1', 'tx2'],
      ['stage', 'int3'],
      ['int3', 'tx3'],
    ]
  })
  await test(t, {
    stageLanguage: 'en',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx2'],
      ['stage', 'int2'],
      ['int2', 'tx1'],
      ['stage', 'int3'],
      ['int3', 'tx3'],
    ]
  })
  await test(t, {
    stageLanguage: 'es',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx3'],
      ['stage', 'int3'],
      ['int3', 'tx1'],
      ['int3', 'int1'],
      ['int1', 'tx2'],
    ]
  })
  await test(t, {
    stageLanguage: 'fr',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx4'],
    ]
  })
});


// Deno.test("really complex configuration", async (t) => {
// })

Deno.test("configuration with duplicated interpreter", async (t) => {

  const elements: Element[] = [
    {
      id: "int1",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'de', provides: 'en' }
      ]
    },
    {
      id: "int2",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'de' }
      ]
    },
    {
      id: "int3",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'es' },
        { accepts: 'de', provides: 'es' },
        { accepts: 'es', provides: 'de' }
      ]
    },
    {
      id: "int4",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'de' }
      ]
    },
    {
      id: "int5",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'en', provides: 'es' }
      ]
    },
    ...TRANSMITTER_DE_EN_ES_FR
  ]
  await test(t, {
    stageLanguage: 'de',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx1'],
      ['stage', 'int1'],
      ['int1', 'tx2'],
      ['stage', 'int3'],
      ['int3', 'tx3'],
      ['int1', 'int5'],
      ['int5', 'tx3']
    ]
  })
  await test(t, {
    stageLanguage: 'en',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx2'],
      ['stage', 'int2'],
      ['int2', 'tx1'],
      ['stage', 'int3'],
      ['int3', 'tx3'],
      ['stage', 'int4'],
      ['int4', 'tx1'],
      ['stage', 'int5'],
      ['int5', 'tx3']
    ]
  })
  await test(t, {
    stageLanguage: 'es',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx3'],
      ['stage', 'int3'],
      ['int3', 'tx1'],
      ['int3', 'int1'],
      ['int1', 'tx2'],
    ]
  })
  await test(t, {
    stageLanguage: 'fr',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'tx4'],
    ]
  })
})
Deno.test("configuration with missing languages (prioritization)", async (t) => {
  const elements: Element[] = [
    {
      id: "int1",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'ar', provides: 'de' }
      ]
    },
    {
      id: "int2",
      type: ElementType.Interpreter,
      langs: [
        { accepts: 'de', provides: 'es' },
        { accepts: 'de', provides: 'en' },
        { accepts: 'de', provides: 'fr' },
      ]
    },
    ...TRANSMITTER_DE_EN_ES_FR
  ]

  await test(t, {
    title: 'english with the highest priority',
    stageLanguage: 'ar',
    elements,
    languagePriorities: defaultLanguagePriorities,
    expectLinks: [
      ['stage', 'int1'],
      ['int1', 'int2'],
      ['int2', 'tx2', 'english has the highest priority, so it is expected over fr and es']
    ]
  })
  await test(t, {
    title: 'spanish with the highest priority',
    stageLanguage: 'ar',
    elements,
    languagePriorities: {
      ...defaultLanguagePriorities,
      en: 0.9,
      fr: 0.9
    },
    expectLinks: [
      ['stage', 'int1'],
      ['int1', 'int2'],
      ['int2', 'tx3', 'spanish has the highest priority, so it is expected over fr and en']
    ]
  })
})

