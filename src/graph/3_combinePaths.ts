import { Path } from '../types.ts'

interface Set {
    i: number,
    paths: Path[],
    cost?: number
}

// returns all possible sets of the provided paths
// criteria:
//  - interpreters can't be duplicated for different languages
export default function getCombinations(v: Path[]): Set[] {
    const res: Set[] = [];

    // each bit position stand for a path and whether this path is used in the current iteration
    for(let i=1;i<Math.pow(2, v.length);i++) {

        const paths: Path[] = []
        const usedInterpreters: {[id: string]: string} = {}
        let storeCombination = true
        // walk though every path and check whether we use it
        for(let elIndex=0;elIndex<v.length;elIndex++) {
            const comp = Math.pow(2,elIndex)

            // bit for this path set?
            if(i & comp) {
                const path = v[elIndex]

                // check whether one of the intepreters is already used
                for(const id in path.usedInterpreters) {
                    if(!usedInterpreters[id] || usedInterpreters[id] == path.usedInterpreters[id]) {
                        // ether not used or used for the same language tuple
                    } else {
                        // already used for a different language tuple, this is not possible ->  don't store this combination
                        storeCombination = false
                        break
                    }
                }
                Object.assign(usedInterpreters, path.usedInterpreters)
                if(storeCombination) {
                    paths.push(path)
                } else {
                    break
                }
            }
        }
        if(storeCombination) {
            res.push({
                i,
                paths
            })
        }
    }

    return res
}