import { LanguagePriorities, Path } from '../types.ts'

/**
 * cost for each missing languages
 * gets multiplied by the language priority
 */
const MISSING_LANG_COST = 10_000

/**
 * cost for each additional node in the relay interpretation path (squared)
 */
const DISTANCE_COST = 100


function arrayAverage(arr: number[]){
  let sum = 0
  for(const i in arr) {
      sum += arr[i]
  }
  return (sum / arr.length)
}

export default function calculateCost(paths: Path[], targetLanguages: string[], languagePriorities: LanguagePriorities) {
    const providedLangs: {[lang: string]: number[]} = {}
    let cost = 0
    for(const p of paths) {
        const distance = Object.keys(p.usedInterpreters).length
        if(providedLangs[p.toLang]) {
            providedLangs[p.toLang].push(distance)
        } else {
            providedLangs[p.toLang] = [distance]
        }
    }

    // for each missing language add some cost
    const missingLangs = targetLanguages.filter(l => !providedLangs[l])
    for(const lang of missingLangs) {
        cost += MISSING_LANG_COST*(languagePriorities[lang] || languagePriorities.default)
    }

    // for each supplied language add its cost
    // based on the distance and redundancy
    for(const lang in providedLangs) {
      const distances = providedLangs[lang]
      const pathCount = distances.length
      const minDistance = Math.min(...distances)
      const avgDistance = arrayAverage(distances)

      cost += Math.pow((minDistance-1), 2) * DISTANCE_COST + avgDistance/pathCount
    }
    return cost
}
