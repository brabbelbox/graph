import buildGraph from './1_buildGraph.ts'
import findPaths from './2_findPaths.ts'
import combinePaths from './3_combinePaths.ts'
import calculateCost from './4_calculateCost.ts'
import reduceElements from './5_reduceElements.ts'

import { Path, Element, ReducedElement, LanguagePriorities } from '../types.ts'

export default function findArrangement(elements: Element[], languagePriorities: LanguagePriorities): ReducedElement[] {
    const rootEl = elements.find(el => el.id == 'stage')
    if(!rootEl) throw new Error('could not find stage element')
    const rootLang = rootEl.langs[0].provides
    if(!rootLang) throw new Error('stage does not provide any language')
    
    // build graph
    const { edges, vertices, targetLanguages } = buildGraph(elements)
    // for each target language:
    // find all possible paths
    const paths: Path[] = []
    for(const targetLang of targetLanguages) {
        paths.push(...findPaths(rootLang, targetLang, vertices))
    }
    // get all possible combinations of these paths
    let sets = combinePaths(paths)

    // calculate and append cost
    let curLowestCost = Infinity
    for(const set of sets) {
        set.cost = calculateCost(set.paths, targetLanguages.filter(l => l !== rootLang), languagePriorities)
        if(set.cost < curLowestCost) curLowestCost = set.cost
    }
    // pick best one
    if(sets.length > 100) sets = sets.filter(r => (r.cost as number) < curLowestCost*1.1)
    const results = sets.sort( (a,b) => (a.cost as number)-(b.cost  as number))
    const bestSet = results[0]
    if(!bestSet) throw new Error('could not find any graph')
    
    // remove all language unused language tuples
    const reducedElements = reduceElements(elements, bestSet.paths)

    return reducedElements
}