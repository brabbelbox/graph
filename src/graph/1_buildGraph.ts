
import { Edge, Vertex, Element, ElementType } from "../types.ts"

export default function buildGraph(elements: Element[]) {
    const edges: Edge[] = []
    const vertices: Vertex[] = []
    const langVertexMap: {[lang: string]: Vertex} = {}

    // collect required languages (=outbound languages like transmitters)
    const targetLanguages: string[] = []
    for(const el of elements){
        // is outbound? (tx, mon, line)
        if([ElementType.Transmitter, ElementType.Monitor, ElementType.LineOut].includes(el.type)) {
            if(el.langs.length == 1) {
                // just one accepted language? then we definitely need it!
                if(el.langs[0].accepts) targetLanguages.push(el.langs[0].accepts)
            } else {
                // otherwise we create a vertex (marked as required)
                // and edges for each accepted lang
                // let's call them "virtual languages"
                const v: Vertex = {
                    lang: el.id,
                    outboundEdges: [],
                    inboundEdges: []
                }
                langVertexMap[v.lang] = v
                vertices.push(v)
                if(!targetLanguages.includes(v.lang)) targetLanguages.push(v.lang)
            }
        }
    }

    // create graph based on the elements
    for(const el of elements) {
        for(const l of el.langs) {

            // first time that we see this language?
            // -> add them as vertices
            if(l.accepts && !langVertexMap[l.accepts]) {
                const v: Vertex = {
                    lang: l.accepts,
                    outboundEdges: [],
                    inboundEdges: []
                }
                langVertexMap[l.accepts] = v
                vertices.push(v)
            }
            if(l.provides && !langVertexMap[l.provides]) {
                const v: Vertex = {
                    lang: l.provides,
                    outboundEdges: [],
                    inboundEdges: []
                }
                langVertexMap[l.provides] = v
                vertices.push(v)
            }

            if(l.accepts && l.provides) {
                // interpreter? add as edge!
                const e: Edge = {
                    fromLang: l.accepts,
                    toLang: l.provides,
                    viaInterpreter: el.id
                }
                edges.push(e)
                langVertexMap[l.accepts].outboundEdges.push(e)
                langVertexMap[l.provides].inboundEdges.push(e)
            }
            // maybe the element provides "virtual languages"?
            if(langVertexMap[el.id] && l.accepts) {
                const e: Edge = {
                    fromLang: l.accepts,
                    toLang: el.id,
                    viaInterpreter: el.id
                }
                edges.push(e)
                langVertexMap[l.accepts].outboundEdges.push(e)
                langVertexMap[el.id].inboundEdges.push(e)
            }
        }
    }
    return {
        edges,
        vertices,
        targetLanguages
    }
}
