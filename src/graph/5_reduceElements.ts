import { Path, Element } from '../types.ts'

export default function reduceElements(elements: Element[], paths: Path[])  {
    const activeInterpreters: {[id: string]: {accepts: string, provides: string}} = {}
    
    for(const path of paths) {
        for(const id in path.usedInterpreters) {
            const [accepts,provides] = path.usedInterpreters[id].split(':')
            activeInterpreters[id] = {accepts,provides}
        }
    }
    
    const reducedElements = elements.map(e => {
        // deno-lint-ignore no-explicit-any
        let lang: any
        if(activeInterpreters[e.id]) {
            lang = e.langs.find(lt => {
                if(lt.accepts && lt.accepts !== activeInterpreters[e.id].accepts) return false
                if(lt.provides && lt.provides !== activeInterpreters[e.id].provides) return false
                return true
            })
        } else {
            lang = e.langs[0]
        }
        if(lang?.accepts && lang?.provides && !activeInterpreters[e.id]) {
            // interpreter is not active
            lang = null
        }
        return {
            id: e.id,
            accepts: lang?.accepts,
            provides: lang?.provides
        }
    })
    return reducedElements
}

