// deno-lint-ignore-file no-explicit-any
import findArrangement from './graph/index.ts'
import { elements, languagePriorities } from './data.ts'

const start = Date.now()

// find best graph arrangement
const reducedElements = findArrangement(elements, languagePriorities)

// collect provider
const providers: {[lang: string]: string[]} = {}
for(const el of reducedElements) {
    if(!el.provides) continue
    if(!providers[el.provides]) providers[el.provides] = []
    if(!providers[el.provides].includes(el.id)) providers[el.provides].push(el.id)
}

// map input to outputs
const outputMapping: {[output:string]: {[input:string]: number}} = {}
for(const el of reducedElements) {
    if(el.accepts) {
        outputMapping[el.id] = providers[el.accepts].reduce((o,input) => {
            o[input] = 1.0
            return o
        }, {} as any)
    }
}

// add background stage sound
for(let id in outputMapping) {
    if(id.match(/^tx/) && !outputMapping[id].stage) {
        outputMapping[id].stage = 0.2
    }
}

console.log({
    elements: reducedElements,
    outputMapping
})

console.log(`execution time: ${Date.now()-start} ms`)