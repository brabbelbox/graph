import { Element } from './types.ts'
export const languagePriorities = {
    DE: 1.2,
    EN: 1.2,
    FR: 1.1,
    ES: 1,
    AR: 0.9,
    sign1: 0.5,
    sign2: 0.5,
}

export const elements: Element[] = [
    {
        id: 'stage',
        langs: [
            {
                provides: 'AR'
            }
        ]
    },
    {
        id: 'int1',
        langs: [
            {
                accepts: 'EN',
                provides: 'DE',
                priority: 2,
            },
            {
                accepts: 'DE',
                provides: 'EN',
                priority: 2,
            },
            {
                accepts: 'DE',
                provides: 'ES',
                priority: 1
            }
        ]
    },
    {
        id: 'int2',
        langs: [
            {
                accepts: 'EN',
                provides: 'ES'
            },
            {
                accepts: 'ES',
                provides: 'DE'
            }
        ]
    },
    {
        id: 'int3',
        langs: [
            {
                accepts: 'DE',
                provides: 'FR'
            },
            {
                accepts: 'FR',
                provides: 'DE'
            }
        ]
    },
    {
        id: 'int4',
        langs: [
            {
                accepts: 'FR',
                provides: 'AR'
            },
            {
                accepts: 'EN',
                provides: 'AR'
            },
            {
                accepts: 'EN',
                provides: 'FR'
            }
        ]
    },
    {
        id: 'int5',
        langs: [
            {
                accepts: 'FR',
                provides: 'AR'
            },
            {
                accepts: 'AR',
                provides: 'FR'
            }
        ]
    },
    // {
    //     id: 'int6',
    //     langs: [
    //     ]
    // },
    {
        id: 'sign1',
        langs: [
            {
                accepts: 'EN'
            },
            {
                accepts: 'DE'
            }
        ]
    },
    {
        id: 'tx1',
        langs: [
            {
                accepts: 'EN'
            }
        ]
    },
    {
        id: 'tx2',
        langs: [
            {
                accepts: 'DE'
            }
        ]
    },
    {
        id: 'tx3',
        langs: [
            {
                accepts: 'ES'
            }
        ]
    },
    {
        id: 'tx4',
        langs: [
            {
                accepts: 'FR'
            }
        ]
    },
    {
        id: 'tx5',
        langs: [
            {
                accepts: 'AR'
            }
        ]
    },
    
]